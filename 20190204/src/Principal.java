
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Declaracion de variables
		//variable entera
		int edad = 21;
		
		//variable decimal
		double altura = 1.70;
		
		//variable caracter
		char estadoCivil = 's';

		//variable cadenas
		String nombre = "Alvaro Serrano Lozano";
		
		//variable booleana
		boolean madrileño = false;
		
		//sysout + control espacio
		System.out.println(edad);
		System.out.println(altura);
		System.out.println("Tu edad es:\t"+edad);
		
		//si tienes 35 y mides 1.79
		if(edad == 35 && altura == 1.79) {
			System.out.println("Eres JC");
		}
		else {
			System.out.println("No eres JC");
		}
		//si mides 1.70 o no eres de madrid, entras gratis
		if (altura > 1.70 || madrileño==false) {
			System.out.println("Entro gratis");
		}
		else {
			System.out.println("A pagar");
		}
		//si estas casado
		//si mides mayor o igual a 1.75
		//si no eres de madrid... regalo viaje a punta cana
		if (estadoCivil =='c') {
			if (altura >= 1.75) {
				if (madrileño == false) {
					System.out.println("Viaje a punta cana");
				}
				else {
					System.out.println("Lastima de haber nacido en Parla");
				}
			}
			else {
				System.out.println("Por 1 cm nada mas!");
			}
		}
		else {
			System.out.println("Ella dijo NO");
		}
	}

}
