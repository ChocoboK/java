import java.net.*;
import java.io.*;

public class Rename {
	public static void main(String[] args) throws Exception {
		
		String palabra = "dog";
		
		URL direccion = new URL("https://dictionary.cambridge.org/es/diccionario/ingles-espanol/" + palabra );
		URL direccion1 = new URL("https://dictionary.cambridge.org/es/diccionario/ingles-frances/" + palabra );
		String html = obtenerHTML (direccion);
		String html1 = obtenerHTML (direccion1);
		String palabraTraducida = cortarURL(html);
		//System.out.println(html);
		System.out.println("\n" + palabraTraducida);
		String palabraTraducida1 = cortarURL1(html1);
		//System.out.println(html1);
		System.out.println("\n" + palabraTraducida1);		
	
	}
	
	public static String obtenerHTML (URL direccionWeb) throws Exception {
		
		BufferedReader in = new BufferedReader(new InputStreamReader(direccionWeb.openStream()));
		String inputLine, codigo="";

		while ((inputLine = in.readLine()) != null)
			codigo+=inputLine;

		in.close();
		
		return codigo;
	}
	
	public static String cortarURL (String html) {		
		int ini = html.indexOf("\"traducir ") + 10;
		int fin = html.indexOf(". ");
		return html.substring(ini, fin);
	}
		
	public static String cortarURL1 (String html1) {		
		int ini = html1.indexOf("\"traducir ") + 10;
		int fin = html1.indexOf(". ");
		return html1.substring(ini, fin);
	}
	
		
}
