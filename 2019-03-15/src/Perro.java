
public class Perro extends Animal {

	//Atributos de Perro
	String raza;
	boolean ladra;
	String tipo; // tipo de pelo, liso, rizado...
	//metodo: si ladra y tiene mas de 10 a�os = chucho asqueroso
	
	//Constructoras de Perro
	public Perro(String nombre, int edad, boolean chip, double peso, String color, String raza, boolean ladra,
			String tipo) {
		super(nombre, edad, chip, peso, color);
		this.raza = raza;
		this.ladra = ladra;
		this.tipo = tipo;
	}

	//Getters & Setters de Perro
	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public boolean isLadra() {
		return ladra;
	}

	public void setLadra(boolean ladra) {
		this.ladra = ladra;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	//toString de Perro
	@Override
	public String toString() {
		return "Perro [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", raza=" + raza + ", ladra=" + ladra + ", tipo=" + tipo + "]";
	}
	
	
}
