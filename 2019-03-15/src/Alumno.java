
public class Alumno {
	// DNI, Nombre, Apellido, Edad, Sexo, FechaNacimiento, Matriculado
	String dni;
	String nombre;
	String apellido;
	int edad;
	char sexo; // H = hombre, M = mujer, O = otro;
	String fechanacimiento;
	boolean matriculado;
	double beca;
	
	public Alumno(String dni, String nombre, String apellido, int edad, char sexo, String fechanacimiento,
			boolean matriculado, double beca) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.sexo = sexo;
		this.fechanacimiento = fechanacimiento;
		this.matriculado = matriculado;
		this.beca = beca;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public String getFechanacimiento() {
		return fechanacimiento;
	}

	public void setFechanacimiento(String fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}

	public boolean isMatriculado() {
		return matriculado;
	}

	public void setMatriculado(boolean matriculado) {
		this.matriculado = matriculado;
	}

	public double getBeca() {
		return beca;
	}

	public void setBeca(double beca) {
		this.beca = beca;
	}

	@Override
	public String toString() {
		return "Alumno [dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + ", sexo="
				+ sexo + ", fechanacimiento=" + fechanacimiento + ", matriculado=" + matriculado + ", beca=" + beca
				+ "]";
	}

}
