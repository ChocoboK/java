
public class Vehiculo {

	//Matricula, marca, modelo y precio (PADRE)
	String matricula;
	String marca;
	String modelo;
	double precio;
	
	//Constructoras de Vehiculo
	public Vehiculo(String matricula, String marca, String modelo, double precio) {
		this.matricula = matricula;
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
	}
	
	//Getters & Setters de Vehiculo
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	//toString de Vehiculo
	@Override
	public String toString() {
		return "Vehiculo [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ "]";
	}
	
	
}
