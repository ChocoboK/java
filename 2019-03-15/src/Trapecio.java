
public class Trapecio extends Figura{

	//Atributos de Trapecio
	private double bMayor;
	private double bMenor;
	private double altura;
	
	//Constructoras de Trapecio
	public Trapecio(String color, char tamaño, double bMayor, double bMenor, double altura) {
		super(color, tamaño);
		this.bMayor = bMayor;
		this.bMenor = bMenor;
		this.altura = altura;
	}

	//Getters & Setters de Trapecio
	public double getbMayor() {
		return bMayor;
	}

	public void setbMayor(double bMayor) {
		this.bMayor = bMayor;
	}

	public double getbMenor() {
		return bMenor;
	}

	public void setbMenor(double bMenor) {
		this.bMenor = bMenor;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	//toString de Trapecio
	@Override
	public String toString() {
		return "Trapecio [color=" + color + ", tamaño=" + tamaño + ", bMayor=" + bMayor + ", bMenor=" + bMenor
				+ ", altura=" + altura + "]";
	}
	
	//Metodo para calcular el area
	double calcularArea() {
		double area;
		area = getAltura() * ((getbMayor() + getbMenor()) /2);
		return area;
	}
	
}
