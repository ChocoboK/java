
public class Moto extends Vehiculo{
	
	//Atributos de la clase Moto
	int cilindrada;
	int numeroluces;
	boolean radio;
	
	//Constructores de Moto
	public Moto(String matricula, String marca, String modelo, double precio, int cilindrada, int numeroluces,
			boolean radio) {
		super(matricula, marca, modelo, precio);
		this.cilindrada = cilindrada;
		this.numeroluces = numeroluces;
		this.radio = radio;
	}

	//Getters & Setters de Moto
	public int getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(int cilindrada) {
		this.cilindrada = cilindrada;
	}

	public int getNumeroluces() {
		return numeroluces;
	}

	public void setNumeroluces(int numeroluces) {
		this.numeroluces = numeroluces;
	}

	public boolean isRadio() {
		return radio;
	}

	public void setRadio(boolean radio) {
		this.radio = radio;
	}

	//toString de Moto
	@Override
	public String toString() {
		return "Moto [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", cilindrada=" + cilindrada + ", numeroluces=" + numeroluces + ", radio=" + radio + "]";
	}

}
