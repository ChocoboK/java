
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Creacion de un objeto Coche
		Coche Coche1 = new Coche("1234DFW", "Audi", "A3", 21340.65, 'D', 12345678, 5, false);
		Coche Coche2 = new Coche("9876TFC", "Mazda", "Rx8", 34000.00, 'H', 87654321, 5, false);

		Alumno Alumno1 = new Alumno("123456789G", "Antonio", "Tomas", 21, 'H', "1999-05-30", true, 9840.10);
		Alumno Alumno2 = new Alumno("987654321X", "Antonia", "Fernandez", 20, 'M', "1999-04-21", false, 1234.90);
		Alumno Alumno3 = new Alumno("907656321X", "Jesus", "Garcia", 25, 'O', "1999-07-30", true, 3496.90);

		
		int [] array = {1, 3, 4, 7, 9, -2, 0, 7};
		int [] array2 = {1, 3, 4, 7, 9, -2, 0, 7};

		Coche [] arrayDeCoches = {Coche1, Coche2};
		Alumno [] arrayDeAlumnos = {Alumno1, Alumno2, Alumno3};
		
		System.out.println(Coche1);
		System.out.println(Coche2.getPrecio());
		System.out.println(Coche1.getTipo());
		
		Coche1.setPrecio(15000);
		System.out.println(Coche1.getPrecio());
		
		//imprimir array de coches con un for
		for (int i=0; i<arrayDeCoches.length; i++) {
		System.out.println(arrayDeCoches[i]);
		}
		
		//imprimir array de alumnos con un for
		for (int i=arrayDeAlumnos.length-1; i>=0; i--) {
		System.out.println(arrayDeAlumnos[i]);
		}
		
		//matriz
		int [][] matriz = { 
							{1, 3, 0, -2, 7},
						  	{4, 9, 9,  5, 6},
						  	{8, 1, 3,  4, 5}
						  };
	}
}
