
public class Figura {

	//Atributos de Figura (PADRE)
	protected String color;
	protected char tamaño; // G -> Grande / M -> Mediano / P -> Pequeño
	
	//Constructoras de Figura
	public Figura(String color, char tamaño) {
		this.color = color;
		this.tamaño = tamaño;
	}

	//Getters & Setters de Figura
	protected String getColor() {
		return color;
	}

	protected void setColor(String color) {
		this.color = color;
	}

	protected char getTamaño() {
		return tamaño;
	}

	protected void setTamaño(char tamaño) {
		this.tamaño = tamaño;
	}

	//toString de Figura
	@Override
	public String toString() {
		return "Figura [color=" + color + ", tamaño=" + tamaño + "]";
	}
	
}
