
public class Conejo extends Animal{

	//Atributos de Conejo
	int tamaño;
	char pelo; // R rizado / L liso
	boolean campero;
	//metodo: tamaño menos de 20 y es campero = comestible
	
	//Constructoras de Conejo
	public Conejo(String nombre, int edad, boolean chip, double peso, String color, int tamaño, char pelo,
			boolean campero) {
		super(nombre, edad, chip, peso, color);
		this.tamaño = tamaño;
		this.pelo = pelo;
		this.campero = campero;
	}

	//Getters & Setters de Conejo
	public int getTamaño() {
		return tamaño;
	}

	public void setTamaño(int tamaño) {
		this.tamaño = tamaño;
	}

	public char getPelo() {
		return pelo;
	}

	public void setPelo(char pelo) {
		this.pelo = pelo;
	}

	public boolean isCampero() {
		return campero;
	}

	public void setCampero(boolean campero) {
		this.campero = campero;
	}

	//toString de Conejo
	@Override
	public String toString() {
		return "Conejo [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", tamaño=" + tamaño + ", pelo=" + pelo + ", campero=" + campero + "]";
	}
	
}
