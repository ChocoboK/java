import java.util.InputMismatchException;
import java.util.Scanner;

public class Principal4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int eleccion = 0;
		int resultado = 0;
		
		do {
		System.out.println("=================================");
		System.out.println(" ||---------------------------||");
		System.out.println(" ||  1. Aparcar Coche   (5€)  ||");
		System.out.println(" ||---------------------------||");
		System.out.println(" ||  2. Aparcar Moto    (2€)  ||");
		System.out.println(" ||---------------------------||");
		System.out.println(" ||  3. Aparcar Camion  (8€)  ||");
		System.out.println(" ||---------------------------||");
		System.out.println(" ||  4. Ver recaudacion       ||");
		System.out.println(" ||---------------------------||");
		System.out.println(" ||  5. Salir                 ||");
		System.out.println(" ||---------------------------||");
		System.out.println("=================================");
		System.out.println(" \n ");
		System.out.print("Elija una opcion: ");

		Scanner teclado = new Scanner(System.in);
		try {
			eleccion = teclado.nextInt();
		} catch (InputMismatchException error) {
			System.out.println("Buen intento...");
		}
		
			switch (eleccion) {
			case 1:
				System.out.print("Dime tu matricula: ");
				String matricula = teclado.next();
				System.out.println("Esta es la matricula: " + matricula + "\n");
				resultado += 5;
				break;
			case 2:
				System.out.print("Dime tu matricula: ");
				String matriculaM = teclado.next();
				System.out.println("Esta es la matricula: " + matriculaM + "\n");
				resultado += 2;
				break;
			case 3:
				System.out.print("Dime tu matricula: ");
				String matriculaCa = teclado.next();
				System.out.println("Esta es la matricula: " + matriculaCa + "\n");
				resultado += 8;
				break;
			case 4:
				System.out.println("Dinero recaudado: " + resultado + "\n");
				break;
			case 5:
				System.out.println("Que tenga un buen dia! " + "\n");
				break;
			default:
				System.out.println("Esa opcion es erronea, pruebe del (1-5)" + "\n");
				break;
			}
		} while (eleccion != 5);
	}
}
