
public class Animal {
	
	//Atributos de Animal (PADRE)
	String nombre;
	int edad;
	boolean chip;
	double peso;
	String color;
	
	//Constructoras de Animal
	public Animal(String nombre, int edad, boolean chip, double peso, String color) {
		this.nombre = nombre;
		this.edad = edad;
		this.chip = chip;
		this.peso = peso;
		this.color = color;
	}

	//Getters & Setters de Animal
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public boolean isChip() {
		return chip;
	}

	public void setChip(boolean chip) {
		this.chip = chip;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	//toString de Animal
	@Override
	public String toString() {
		return "Animal [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ "]";
	}

}
