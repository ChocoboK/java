public class Camion extends Vehiculo{

	//Atributos de la clase Camion
	double peso;
	int numeroejes;
	boolean remolque;
	double capacidad;
	char señal;
	
	//Constructoras de Camion
	public Camion(String matricula, String marca, String modelo, double precio, double peso, int numeroejes,
			boolean remolque, double capacidad, char señal) {
		super(matricula, marca, modelo, precio);
		this.peso = peso;
		this.numeroejes = numeroejes;
		this.remolque = remolque;
		this.capacidad = capacidad;
		this.señal = señal;
	}
	
	//Getters & Setters de Camion
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public int getNumeroejes() {
		return numeroejes;
	}
	public void setNumeroejes(int numeroejes) {
		this.numeroejes = numeroejes;
	}
	public boolean isRemolque() {
		return remolque;
	}
	public void setRemolque(boolean remolque) {
		this.remolque = remolque;
	}
	public double getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(double capacidad) {
		this.capacidad = capacidad;
	}
	public char getSeñal() {
		return señal;
	}
	public void setSeñal(char señal) {
		this.señal = señal;
	}
	
	//toString de Camion
	@Override
	public String toString() {
		return "Camion [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", peso=" + peso + ", numeroejes=" + numeroejes + ", remolque=" + remolque + ", capacidad="
				+ capacidad + ", señal=" + señal + "]";
	}
	
}
