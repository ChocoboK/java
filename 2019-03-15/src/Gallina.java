
public class Gallina extends Animal{

	//Atributos de Gallina
	int numHuevos;
	boolean tieneCresta;
	//metodo: numero de docenas
	
	//Constructoras de Gallina
	public Gallina(String nombre, int edad, boolean chip, double peso, String color, int numHuevos,
			boolean tieneCresta) {
		super(nombre, edad, chip, peso, color);
		this.numHuevos = numHuevos;
		this.tieneCresta = tieneCresta;
	}

	//Getters & Setters de Gallina
	public int getNumHuevos() {
		return numHuevos;
	}

	public void setNumHuevos(int numHuevos) {
		this.numHuevos = numHuevos;
	}

	public boolean isTieneCresta() {
		return tieneCresta;
	}

	public void setTieneCresta(boolean tieneCresta) {
		this.tieneCresta = tieneCresta;
	}

	//toString
	@Override
	public String toString() {
		return "Gallina [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", numHuevos=" + numHuevos + ", tieneCresta=" + tieneCresta + "]";
	}
	
}
