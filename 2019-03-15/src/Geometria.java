import java.util.InputMismatchException;
import java.util.Scanner;

public class Geometria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int eleccion = 0;

		System.out.println("Elige tu figura:");
		System.out.println("----------------");
		System.out.println("  1- Circulo");
		System.out.println("  2- Cuadrado");
		System.out.println("  3- Triangulo");
		System.out.println("  4- Rombo");
		System.out.println("  5- Trapecio");
		System.out.println("  6- Rectangulo");
		System.out.println("  7- Salir" + "\n");
		System.out.print("Elija una figura: ");

		Scanner teclado = new Scanner(System.in);		
		try {
			eleccion = teclado.nextInt();
		} catch (InputMismatchException error) {
			System.out.println("Buen intento...");
		}

		switch (eleccion) {
		case 1:
			System.out.println("Dime el radio: ");
			double radio = teclado.nextDouble();
			Circulo cir1 = new Circulo("Azul", 'M', radio);
			System.out.println("Area del circulo: " + cir1.calcularArea());
			break;
		case 2:
			System.out.println("Dime el lado: ");
			double lado = teclado.nextDouble();
			Cuadrado cua1 = new Cuadrado("Verde", 'P', lado);
			System.out.println("Area del cuadrado: " + cua1.calcularArea());
			break;
		case 3:
			System.out.println("Dime la base: ");
			double base = teclado.nextDouble();
			System.out.println("Dime la altura: ");
			double altura = teclado.nextDouble();
			Triangulo tri1 = new Triangulo("Azul", 'G', base, altura);
			System.out.println("Area del triangulo: " + tri1.calcularArea());
			break;
		case 4:
			System.out.println("Dime la diagonal mayor: ");
			double dMayor = teclado.nextDouble();
			System.out.println("Dime la diagonal menor: ");
			double dMenor = teclado.nextDouble();
			Rombo rom1 = new Rombo("Morado", 'M', dMayor, dMenor);
			System.out.println("Area del rombo: " + rom1.calcularArea());
			break;
		case 5:
			System.out.println("Dime la base mayor: ");
			double bMayor = teclado.nextDouble();
			System.out.println("Dime la base mayor: ");
			double bMenor = teclado.nextDouble();
			System.out.println("Dime la altura: ");
			double altura1 = teclado.nextDouble();
			Trapecio tra1 = new Trapecio("Amarillo", 'G', bMayor, bMenor, altura1);
			System.out.println("Area del trapecio: " + tra1.calcularArea());
			break;
		case 6:
			System.out.println("Dime la base: ");
			double base1 = teclado.nextDouble();
			System.out.println("Dime la altura: ");
			double altura2 = teclado.nextDouble();
			Rectangulo rec1 = new Rectangulo("Naranja", 'P', base1, altura2);
			System.out.println("Area del rectangulo: " + rec1.calcularArea());
			break;
		case 7:
			System.out.println("Vuelve pronto!");
			break;
		default:
			System.out.println("Esa figura no existe... para calcular areas prueba con 1-6 \nSi no, 7 para salir");

		}

	}

}
