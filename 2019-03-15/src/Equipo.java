
public class Equipo {
	// Nombre, FechaCreacion, nSeguidores, Division, Estadio
	String nombre;
	int fechacreacion;
	int nseguidores;
	char division; // P = primera, S = segunda
	boolean estadio;
	double sueldomedio;
	
	public Equipo(String nombre, int fechacreacion, int nseguidores, char division, boolean estadio, double sueldomedio) {
		super();
		this.nombre = nombre;
		this.fechacreacion = fechacreacion;
		this.nseguidores = nseguidores;
		this.division = division;
		this.estadio = estadio;
		this.sueldomedio = sueldomedio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getFechacreacion() {
		return fechacreacion;
	}

	public void setFechacreacion(int fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	public int getNseguidores() {
		return nseguidores;
	}

	public void setNseguidores(int nseguidores) {
		this.nseguidores = nseguidores;
	}

	public char getDivision() {
		return division;
	}

	public void setDivision(char division) {
		this.division = division;
	}

	public boolean isEstadio() {
		return estadio;
	}

	public void setEstadio(boolean estadio) {
		this.estadio = estadio;
	}

	public double getSueldomedio() {
		return sueldomedio;
	}

	public void setSueldomedio(double sueldomedio) {
		this.sueldomedio = sueldomedio;
	}

	@Override
	public String toString() {
		return "Equipo [nombre=" + nombre + ", fechacreacion=" + fechacreacion + ", nseguidores=" + nseguidores
				+ ", division=" + division + ", estadio=" + estadio + ", sueldomedio=" + sueldomedio + "]";
	}
	
}
