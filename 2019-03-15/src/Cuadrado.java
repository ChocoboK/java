
public class Cuadrado extends Figura{

	//Atributos de Cuadrado
	private double lado;

	//Constructoras de Cuadrado
	public Cuadrado(String color, char tamaño, double lado) {
		super(color, tamaño);
		this.lado = lado;
	}

	//Getters & Setters de Cuadrado
	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}

	//toString de Cuadrado
	@Override
	public String toString() {
		return "Cuadrado [color=" + color + ", tamaño=" + tamaño + ", lado=" + lado + "]";
	}
	
	//Metodo para calcular el area
	public double calcularArea() {
	double area;
		area = Math.pow(getLado(),2);
		return area;
	}
}
