
public class Triangulo extends Figura{

	//Atributos de Triangulo
	private double base;
	private double altura;

	//Constructoras de Triangulo
	public Triangulo(String color, char tamaño, double base, double altura) {
		super(color, tamaño);
		this.base = base;
		this.altura = altura;
	}

	//Getters & Setters de Triangulo
	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	//toString de Triangulo
	@Override
	public String toString() {
		return "Triangulo [color=" + color + ", tamaño=" + tamaño + ", base=" + base + ", altura=" + altura + "]";
	}
	
	//Metodo para calcular el area
	double calcularArea() {
		double area;
		area = (getBase() * getAltura())/2;
		return area;
	}
	
}
