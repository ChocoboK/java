
public class Rombo extends Figura{
	
	//Atributos de Rombo
	private double dMayor;
	private double dMenor;
	
	//Constructoras de Rombo
	public Rombo(String color, char tamaño, double dMayor, double dMenor) {
		super(color, tamaño);
		this.dMayor = dMayor;
		this.dMenor = dMenor;
	}

	//Getters & Setters de Rombo
	public double getdMayor() {
		return dMayor;
	}

	public void setdMayor(double dMayor) {
		this.dMayor = dMayor;
	}

	public double getdMenor() {
		return dMenor;
	}

	public void setdMenor(double dMenor) {
		this.dMenor = dMenor;
	}

	//toString de Rombo
	@Override
	public String toString() {
		return "Rombo [color=" + color + ", tamaño=" + tamaño + ", dMayor=" + dMayor + ", dMenor=" + dMenor + "]";
	}
	
	//Metodo para calcular el area
	double calcularArea() {
		double area;
		area = (getdMayor() * getdMenor())/2;
		return area;
	}


}
