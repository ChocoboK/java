
public class Rectangulo extends Figura{
	
	//Atributos de Rectangulo
	private double base;
	private double altura;
	
	//Constructoras de Rectangulo
	public Rectangulo(String color, char tamaño, double base, double altura) {
		super(color, tamaño);
		this.base = base;
		this.altura = altura;
	}

	//Getters & Setters de Rectangulo
	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	//toString de Rectangulo
	@Override
	public String toString() {
		return "Rectangulo [color=" + color + ", tamaño=" + tamaño + ", base=" + base + ", altura=" + altura + "]";
	}
	
	//Metodo para calcular el area
	double calcularArea() {
		double area;
		area = getBase() * getAltura();
		return area;
	}
	

}
