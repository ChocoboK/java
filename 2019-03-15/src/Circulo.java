
public class Circulo extends Figura{

	//Atributos de Circulo
	double radio;

	//Constructoras de Circulo
	public Circulo(String color, char tamaño, double radio) {
		super(color, tamaño);
		this.radio = radio;
	}

	//Getters & Setters de Circulo
	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	//toString de Circulo
	@Override
	public String toString() {
		return "Circulo [color=" + color + ", tamaño=" + tamaño + ", radio=" + radio + "]";
	}
	
	//Metodo para calcular el area
	double calcularArea() {
		double area;
		area = Math.PI * Math.pow(getRadio(), 2);
		return area;
	}
}
