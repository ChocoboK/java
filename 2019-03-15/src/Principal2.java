
public class Principal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Creacion de un objeto Coche
		Coche Coche1 = new Coche("9870ZFZ", "Mazda", "Rx8", 44000.00, 'H', 87654321, 5, false);
		Moto Moto1 = new Moto("9548XZX", "Yamaha", "G35", 18000, 500, 4, false);
		Camion Camion1 = new Camion("4321094", "Daf", "730T", 85000, 5.5, 6, true, 2000, 'C');
		
		Vehiculo Vehiculo1 = new Coche("2134XCZ", "Seat", "Leon", 15000, 'G', 223, 5, true);
		Vehiculo Vehiculo2 = new Moto("1234KJD", "Honda", "CVR", 33000, 450, 4, true);
		Vehiculo Vehiculo3 = new Camion("9852UTD", "Volvo", "FH16", 115000, 7.5, 6, false, 3500, 'B');
		
		System.out.println(Coche1 + "\n" + Moto1 + "\n" + Camion1);
		System.out.println("\n");
		System.out.println(Vehiculo1 + "\n" + Vehiculo2 + "\n" + Vehiculo3);

	}

}
