
public class Coche extends Vehiculo {
	
	//Atributos de la clase coche
	char tipo;
	int bastidor;
	int ruedas;
	boolean descapotable;
	
	//Constructoras de Coche, super se trae las variales del padre Vehiculo.
	public Coche(String matricula, String marca, String modelo, double precio, char tipo, int bastidor, int ruedas,
			boolean descapotable) {
		super(matricula, marca, modelo, precio);
		this.tipo = tipo;
		this.bastidor = bastidor;
		this.ruedas = ruedas;
		this.descapotable = descapotable;
	}

	//Getters & Setters de Coche
	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public int getBastidor() {
		return bastidor;
	}

	public void setBastidor(int bastidor) {
		this.bastidor = bastidor;
	}

	public int getRuedas() {
		return ruedas;
	}

	public void setRuedas(int ruedas) {
		this.ruedas = ruedas;
	}

	public boolean isDescapotable() {
		return descapotable;
	}

	public void setDescapotable(boolean descapotable) {
		this.descapotable = descapotable;
	}

	//toString de Coche
	@Override
	public String toString() {
		return "Coche [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", tipo=" + tipo + ", bastidor=" + bastidor + ", ruedas=" + ruedas + ", descapotable=" + descapotable
				+ "]";
	}

}
