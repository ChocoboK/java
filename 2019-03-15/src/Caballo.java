
public class Caballo extends Animal{

	//Atributos de Caballo
	double altura;
	boolean salvaje;
	char sexo; // M = Macho / H = Hembra
	//metodo: si Hembra y edad mas de 20 a�os, puede parir
	
	//Constructoras de Caballo
	public Caballo(String nombre, int edad, boolean chip, double peso, String color, double altura, boolean salvaje,
			char sexo) {
		super(nombre, edad, chip, peso, color);
		this.altura = altura;
		this.salvaje = salvaje;
		this.sexo = sexo;
	}

	//Getters & Setters de Caballo
	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public boolean isSalvaje() {
		return salvaje;
	}

	public void setSalvaje(boolean salvaje) {
		this.salvaje = salvaje;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	//toString
	@Override
	public String toString() {
		return "Caballo [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", altura=" + altura + ", salvaje=" + salvaje + ", sexo=" + sexo + "]";
	}
	
}
