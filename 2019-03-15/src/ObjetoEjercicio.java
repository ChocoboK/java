
public class ObjetoEjercicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Alumno Alumno1 = new Alumno("123456789G", "Antonio", "Tomas", 21, 'H', "1999-05-30", true, 9840.10);
		Alumno Alumno2 = new Alumno("987654321X", "Antonia", "Fernandez", 20, 'M', "1999-04-21", false, 1234.90);
		
		Equipo Equipo1 = new Equipo("Betis", 1970, 9000, 'P', true, 8765.30);
		Equipo Equipo2 = new Equipo("R.Vallecano", 1973, 8760, 'S', false, 1234.50);
		
		System.out.println(Alumno1);
		System.out.println(Equipo1);
		
		System.out.println(Alumno2);
		System.out.println(Equipo2);
	}

}
